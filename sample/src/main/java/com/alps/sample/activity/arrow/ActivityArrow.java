package com.alps.sample.activity.arrow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alps.sample.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityArrow extends AppCompatActivity {

	public static final String EXTRAS_DEVICES = "DEVICES";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_arrow);

		//センサー名取り出し
		List<String> nameList = getIntent().getStringArrayListExtra(EXTRAS_DEVICES);
		//センサー名表示
		ListView nameListView = (ListView) findViewById(R.id.nameList);
		BaseAdapter nameAdapter = new NameAdapter(ActivityArrow.this, nameList, 1);
		nameListView.setAdapter(nameAdapter);

		int interval = 1;
		//データの取り出し
		DataStock dataStock = new DataStock(this);
		List<DataStock.ResultData> dataListAll = dataStock.dataSelectAll();
		List<DataStock.ResultData> dataList = new ArrayList<>();
		for (DataStock.ResultData data : dataListAll) {
			if(data.getLatestData().accZ != 0){
				dataList.add(data);
			}
		}
		@SuppressLint("UseSparseArrays")
		Map<Integer, Calendar> calendarMap = new HashMap<>();
		Calendar firstCalender = Calendar.getInstance();
		for (int i = 0; i < dataList.size(); i++) {
			Calendar calendar = Calendar.getInstance();
			DataStock.ResultData data = dataList.get(i);
			calendar.set(data.getLatestData().year, data.getLatestData().month - 1,
					data.getLatestData().day, data.getLatestData().hour,
					data.getLatestData().minute, data.getLatestData().second);
			//最過去を取得
			if(calendar.before(firstCalender)){
				firstCalender = (Calendar) calendar.clone();
			}
			calendarMap.put(i, calendar);
		}
		Calendar nowDate = Calendar.getInstance();
		Calendar thenDate = firstCalender;

		List<List<Float>> dataFloatList = new ArrayList<>();
		List<String> dataTimeText = new ArrayList<>();
		while (thenDate.before(nowDate)) {
			List<Float> dataLine = new ArrayList<>();
			Calendar nextDate = (Calendar) thenDate.clone();
			nextDate.add(Calendar.SECOND, interval);
			for (String name : nameList) {
				List<Integer> targetNumber = new ArrayList<>();
				for (int i = 0; i < calendarMap.size(); i++) {
					if ((calendarMap.get(i).after(thenDate) || calendarMap.get(i).equals(thenDate))
							&& calendarMap.get(i).before(nextDate)
							&& name.equals(dataList.get(i).getName())) {
						targetNumber.add(i);
					}
				}
				float sumAngle = 0;
				for (int i : targetNumber) {
					sumAngle += dataList.get(i).getLatestData().accZ;
				}
				float aveAngle = targetNumber.size() == 0 ? 0 : sumAngle / targetNumber.size();
				dataLine.add(aveAngle);
			}
			thenDate = nextDate;
			dataTimeText.add(thenDate.get(Calendar.HOUR_OF_DAY) + ":" +
					thenDate.get(Calendar.MINUTE) + ":" + thenDate.get(Calendar.SECOND));
			dataFloatList.add(dataLine);
		}

		List<List<String>> dataStringList = new ArrayList<>();
		for (List<Float> dataFloat : dataFloatList) {
			List<String> dataString = new ArrayList<>();
			for (float i : dataFloat){
				dataString.add(i == 0 ? "-" : i > 0.5 ? "↑": i < -0.5 ? "↓" : "→");
			}
			dataStringList.add(dataString);
		}
		//データの表示
		LinearLayout dataListView = (LinearLayout) findViewById(R.id.dataList);
		LayoutInflater layoutInflater =
				(LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < dataTimeText.size(); i++) {
			BaseAdapter dataAdapter = new NameAdapter(ActivityArrow.this, dataStringList.get(i), 2);
			RelativeLayout root = new RelativeLayout(this);
			if (layoutInflater != null) {
				View view = layoutInflater.inflate(R.layout.custom_arrow_time, root);
				((TextView)view.findViewById(R.id.timeTextView)).setText(dataTimeText.get(i));
				((ListView)view.findViewById(R.id.listView)).setAdapter(dataAdapter);
				dataListView.addView(view);

			}
		}
	}


	private class NameAdapter<T> extends BaseAdapter {
		private final int category;
		private LayoutInflater layoutInflater;
		private List<T> nameList;

		private NameAdapter(Context context, List<T> nameList, int category) {
			this.layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.nameList = nameList;
			this.category = category;
		}

		@Override
		public int getCount() {
			return nameList.size();
		}

		@Override
		public Object getItem(int i) {
			return nameList.get(i);
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@SuppressLint("ViewHolder")
		@Override
		public View getView(int i, View view, ViewGroup viewGroup) {
			if (category == 1) {
				view = layoutInflater.inflate(R.layout.custom_arrow_name, viewGroup, false);
			} else {
				view = layoutInflater.inflate(R.layout.custom_arrow_cell, viewGroup, false);
			}
			((TextView)view.findViewById(R.id.textView)).setText(nameList.get(i).toString());
			return view;
		}
	}
}