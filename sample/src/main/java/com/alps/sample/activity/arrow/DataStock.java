package com.alps.sample.activity.arrow;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.alps.sample.sensorModule.LatestData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class DataStock {
	private SQLiteOpenHelper dataStockHelper;

	public DataStock(Context context){
		dataStockHelper = new DataStockHelper(context);
	}

	public int dataInsert(String name, LatestData data){
		SQLiteDatabase database = dataStockHelper.getReadableDatabase();
		int result;
		ContentValues values = new ContentValues();
		Calendar calendar = Calendar.getInstance();
		values.put(DataStockHelper.NAME, name);
		values.put(DataStockHelper.MAG_X, data.magX);
		values.put(DataStockHelper.MAG_Y, data.magY);
		values.put(DataStockHelper.MAG_Z, data.magZ);
		values.put(DataStockHelper.ACC_X, data.accX);
		values.put(DataStockHelper.ACC_Y, data.accY);
		values.put(DataStockHelper.ACC_Z, data.accZ);
		values.put(DataStockHelper.PRESSURE, data.pressure);
		values.put(DataStockHelper.HUMIDITY, data.humidity);
		values.put(DataStockHelper.TEMPERATURE, data.temperature);
		values.put(DataStockHelper.UV, data.uv);
		values.put(DataStockHelper.AMBIENTLIGHT, data.ambientLight);
		values.put(DataStockHelper.YEAR, calendar.get(Calendar.YEAR));
		values.put(DataStockHelper.MONTH, calendar.get(Calendar.MONTH) + 1);
		values.put(DataStockHelper.DAY, calendar.get(Calendar.DAY_OF_MONTH));
		values.put(DataStockHelper.HOUR, calendar.get(Calendar.HOUR_OF_DAY));
		values.put(DataStockHelper.MINUTE, calendar.get(Calendar.MINUTE));
		values.put(DataStockHelper.SECOND, calendar.get(Calendar.SECOND));
		values.put(DataStockHelper.MS, calendar.get(Calendar.MILLISECOND));
		result = (int) database.insert(DataStockHelper.TABLE_NAME, null, values);
		database.close();
		return result;
	}

	public List<ResultData> dataSelectAll(){
		List<ResultData> dataList = new ArrayList<>();
		SQLiteDatabase database = dataStockHelper.getReadableDatabase();
		Cursor cursor = database.query(DataStockHelper.TABLE_NAME,
				new String[]{DataStockHelper.NAME,
						DataStockHelper.MAG_X,
						DataStockHelper.MAG_Y,
						DataStockHelper.MAG_Z,
						DataStockHelper.ACC_X,
						DataStockHelper.ACC_Y,
						DataStockHelper.ACC_Z,
						DataStockHelper.PRESSURE,
						DataStockHelper.HUMIDITY,
						DataStockHelper.TEMPERATURE,
						DataStockHelper.UV,
						DataStockHelper.AMBIENTLIGHT,
						DataStockHelper.YEAR,
						DataStockHelper.MONTH,
						DataStockHelper.DAY,
						DataStockHelper.HOUR,
						DataStockHelper.MINUTE,
						DataStockHelper.SECOND,
						DataStockHelper.MS
				},
				null,
				null,
				null,
				null,
				DataStockHelper.YEAR + "," + DataStockHelper.MONTH + "," +
						DataStockHelper.DAY + "," + DataStockHelper.HOUR + "," +
						DataStockHelper.MINUTE + "," + DataStockHelper.SECOND + "," +
						DataStockHelper.MS);
		while (cursor.moveToNext()) {
			LatestData latestData = new LatestData();
			String name = cursor.getString(cursor.getColumnIndex(DataStockHelper.NAME));
			latestData.magX = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.MAG_X));
			latestData.magY = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.MAG_Y));
			latestData.magZ = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.MAG_Z));
			latestData.accX = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.ACC_X));
			latestData.accY = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.ACC_Y));
			latestData.accZ = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.ACC_Z));
			latestData.pressure = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.PRESSURE));
			latestData.humidity = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.HUMIDITY));
			latestData.temperature = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.TEMPERATURE));
			latestData.uv = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.UV));
			latestData.ambientLight = cursor.getFloat(cursor.getColumnIndex(DataStockHelper.AMBIENTLIGHT));
			latestData.year = cursor.getInt(cursor.getColumnIndex(DataStockHelper.YEAR));
			latestData.month = cursor.getInt(cursor.getColumnIndex(DataStockHelper.MONTH));
			latestData.day = cursor.getInt(cursor.getColumnIndex(DataStockHelper.DAY));
			latestData.hour = cursor.getInt(cursor.getColumnIndex(DataStockHelper.HOUR));
			latestData.minute = cursor.getInt(cursor.getColumnIndex(DataStockHelper.MINUTE));
			latestData.second = cursor.getInt(cursor.getColumnIndex(DataStockHelper.SECOND));
			latestData.ms = cursor.getInt(cursor.getColumnIndex(DataStockHelper.MS));
			dataList.add(new ResultData(name, latestData));
		}
		cursor.close();
		database.close();
		return dataList;
	}

	public class ResultData{
		private String name;
		private LatestData latestData;

		private ResultData(String name, LatestData latestData) {
			this.name = name;
			this.latestData = latestData;
		}

		public String getName() {
			return name;
		}

		public LatestData getLatestData() {
			return latestData;
		}
	}

	public int removeAll(){
		int result;
		SQLiteDatabase database = dataStockHelper.getReadableDatabase();
		result = database.delete(DataStockHelper.TABLE_NAME, null, null);
		database.close();
		return result;
	}
}
