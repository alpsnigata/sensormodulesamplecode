package com.alps.sample.activity.arrow;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataStockHelper extends SQLiteOpenHelper{
	private static final String DB_NAME = "bledata.db";
	private static final int DB_VERSION = 1;
	public static final String TABLE_NAME = "bledata";
	public static final String NAME = "name";
	public static final String MAG_X = "magx";
	public static final String MAG_Y = "magy";
	public static final String MAG_Z = "magz";
	public static final String ACC_X = "accx";
	public static final String ACC_Y = "accy";
	public static final String ACC_Z = "accz";
	public static final String PRESSURE = "pressure";
	public static final String HUMIDITY = "humidity";
	public static final String TEMPERATURE = "temperature";
	public static final String UV = "uv";
	public static final String AMBIENTLIGHT = "ambientLight";
	public static final String YEAR = "year";
	public static final String MONTH = "month";
	public static final String DAY = "day";
	public static final String HOUR = "hour";
	public static final String MINUTE = "minute";
	public static final String SECOND = "second";
	public static final String MS = "ms";

	public DataStockHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		String createTable = "CREATE TABLE " + TABLE_NAME + " ( " +
				NAME + " TEXT, " +
				MAG_X + " NUMERIC, " +
				MAG_Y + " NUMERIC, " +
				MAG_Z + " NUMERIC, " +
				ACC_X + " NUMERIC, " +
				ACC_Y + " NUMERIC, " +
				ACC_Z + " NUMERIC, " +
				PRESSURE + " NUMERIC, " +
				HUMIDITY + " NUMERIC, " +
				TEMPERATURE + " NUMERIC, " +
				UV + " NUMERIC, " +
				AMBIENTLIGHT + " NUMERIC, " +
				YEAR + " INTEGER, " +
				MONTH + " INTEGER, " +
				DAY + " INTEGER, " +
				HOUR + " INTEGER, " +
				MINUTE + " INTEGER, " +
				SECOND + " INTEGER, " +
				MS + " INTEGER " +
				" ) ";
		sqLiteDatabase.execSQL(createTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

	}


}
